# NLP Review Classifier

Creating classifiers using NLTK's Naive-Bayes classifier to attempt to predict the score of Amazon Reviews

Instructions on how to get this to run:
1.  First run gz_json_coversion.py - this file will format the reviews properly for use later on
2.  Run train_test_generator.py - this file will generate training and testing data from the reveiws created by gz_json_conversion.py
3.  Finally, run create_featureset_and_classify.py - this will actually create, train, and test the classifiers

Note that every time you run train_test_generator.py the reviews are shuffled into a different order and
so different reveiws will be used the next time you run create_featureset_and_classify.py. This will
lead to different accuracies than the previous time you ran the program. This is due to the small
sample size of reviews that are used since the classifier does not handle large amounts of data well.