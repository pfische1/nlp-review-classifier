# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 14:07:55 2019

@author: ptfis
"""

# Purpose of this file is to take a set of amazon reviews from
# http://jmcauley.ucsd.edu/data/amazon/
# and parse them into files containing positive, negative, and individual star
# reveiws

import gzip
import json


# return json of review from a review file
def parse(path):
    with gzip.open(path, 'r') as g:
        for l in g:
            yield(json.loads(l))


def parse_reviews(filename, abbreviation):
    # this function takes reveiws from filename and parses them into
    # files with abbreviation specified by abbreviation
    a = abbreviation + '_reviews.json'
    # create positive, negative, and individual star review files
    reviews = open(a, 'w')
    one = open('one_' + a, 'w')
    two = open('two_' + a, 'w')
    three = open('three_' + a, 'w')
    four = open('four_' + a, 'w')
    five = open('five_' + a, 'w')
    neg = open('neg_' + a, 'w')
    pos = open('pos_' + a, 'w')
    txt_only = open(abbreviation + '_reviews_text_only.txt', 'w')

    amounts = [0, 0, 0, 0, 0]   # used for debugging and seeing amounts
    
    pos_reviews = 0
    neg_reviews = 0
    
    # parse all reviews from main review file
    for l in parse(filename):
        # ignore reviews with no text
        if l['reviewText'] == "":
            continue
    
        # create document with just text of reviews
        txt_only.write(l['summary'].lower() + ' ' + l['reviewText'].lower() + '\n')
    
        # create positive, 4, and 5 star reveiw files
        if l['overall'] > 3.0:
            r = {'class': 'pos', 'overall': l['overall'], 'asin': l['asin'],
                 'summary': l['summary'].lower(),
                 'reviewText': l['reviewText'].lower()}
            if l['overall'] == 5:
                r2 = {'class': '5', 'overall': l['overall'], 'asin': l['asin'],
                      'summary': l['summary'].lower(),
                      'reviewText': l['reviewText'].lower()}
                five.write(json.dumps(r2) + '\n')
                amounts[4] += 1
            else:
                r2 = {'class': '4', 'overall': l['overall'], 'asin': l['asin'],
                      'summary': l['summary'].lower(),
                      'reviewText': l['reviewText'].lower()}
                four.write(json.dumps(r2) + '\n')
                amounts[3] += 1
            reviews.write(json.dumps(r) + '\n')
            pos.write(json.dumps(r) + '\n')
            pos_reviews += 1

        # create negative, 1, and 2 star review files
        elif l['overall'] < 3.0:
            r = {'class': 'neg', 'overall': l['overall'], 'asin': l['asin'],
                 'summary': l['summary'].lower(),
                 'reviewText': l['reviewText'].lower()}
            if l['overall'] == 2:
                r2 = {'class': '2', 'overall': l['overall'], 'asin': l['asin'],
                      'summary': l['summary'].lower(),
                      'reviewText': l['reviewText'].lower()}
                two.write(json.dumps(r2) + '\n')
                amounts[1] += 1
            else:
                r2 = {'class': '1', 'overall': l['overall'], 'asin': l['asin'],
                      'summary': l['summary'].lower(),
                      'reviewText': l['reviewText'].lower()}
                one.write(json.dumps(r2) + '\n')
                amounts[0] += 1
            reviews.write(json.dumps(r) + '\n')
            neg.write(json.dumps(r) + '\n')
            neg_reviews += 1

        # create 3 star reveiw files
        else:
            r2 = {'class': '3', 'overall': l['overall'], 'asin': l['asin'],
                  'summary': l['summary'].lower(),
                  'reviewText': l['reviewText'].lower()}
            three.write(json.dumps(r2) + '\n')
            amounts[2] += 1

    # close files
    reviews.close()
    pos.close()
    neg.close()
    one.close()
    two.close()
    three.close()
    four.close()
    five.close()
    txt_only.close()


if __name__ == '__main__':
    # the review file is named 'reviews_Automotive_5.json.gz' and the files
    # that are generated will use the abbreviation 'auto'
    parse_reviews('reviews_Automotive_5.json.gz', 'auto')
