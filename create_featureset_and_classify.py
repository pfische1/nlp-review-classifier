# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 15:23:55 2019

@author: ptfis
"""
# the purpose of this file is to take a set of reveiws and attempt to train
# a classifier to predict whether the reveiw is positive or negative. Multiple
# classifiers are trained as they can have different strengths or accuracies.
# When training the classifier, the training file and testing file will have
# an equal number of each category. For example, if there are only 500 reviews
# in the negative category but there are 1000 in the positive category, only
# 500 of the positive category will be used to train the model.


import nltk
from nltk.tokenize import word_tokenize
import string
import time


# set up list of tokens that 'negate' the word that comes after it
negations = ["n't", 'never', 'no', 'not']

# set up list of stop words. Note that I do not use the nltk list of stop words
# as that included some of the negation words and I fould using this list gave
# higher accuracy.
stop_words = ['a', 'about', 'above', 'after', 'again', 'ain',
              'all', 'am', 'an', 'and', 'any', 'are', 'as',
              'at', 'be', 'because', 'been', 'before', 'being', 'below',
              'between', 'both', 'but', 'by', 'can',
              'd', 'did', 'do',
              'doing', 'down', 'during', 'each', 'few', 'for',
              'from', 'further', 'had', 'has',
              'have', 'having', 'he', 'her',
              'here', 'hers', 'herself', 'him', 'himself', 'his', 'how',
              'i', 'if', 'in', 'into', 'is', 'it', "it's",
              'its', 'itself', 'just', 'll', 'm', 'ma', 'me', 'mightn',
              'more', 'most', 'my', 'myself',
              'now', 'o', 'of', 'off',
              'on', 'once', 'only', 'or', 'other', 'our', 'ours', 'ourselves',
              'out', 'over', 'own', 're', 's', 'same', 'shan',
              'she', "she's", 'should', "should've",
              'so', 'some', 'such', 't', 'than', 'that', "that'll", 'the',
              'their', 'theirs', 'them', 'themselves', 'then', 'there',
              'these', 'they', 'this', 'those', 'through', 'to', 'too',
              'until', 'up', 've', 'very', 'was',
              'we', 'were', 'what', 'when', 'where',
              'which', 'while', 'who', 'whom', 'why', 'will', 'with',
              'y', 'you', "you'd", "you'll",
              "you're", "you've", 'your', 'yours', 'yourself', 'yourselves']


# replace punctuation with spaces. I found that removing punctuation from the
# text when determining what words are the most frequent increased accuracy
table = str.maketrans({key: ' ' for key in string.punctuation})

# create sets of positive and negative sentiment words
pos_sent = []
neg_sent = []

with open('pos_words.txt', 'r') as pw:
    for line in pw:
        pos_sent.append(line.lower().strip())

with open('neg_words.txt', 'r') as nw:
    for line in nw:
        neg_sent.append(line.lower().strip())

sent_words = pos_sent + neg_sent
sent_words = set(sent_words)

print('set up sent_words')


def file_length(filename):
    # get the number of lines in a file. Used to find out how many reviews
    # the file contains
    with open(filename) as f:
        for i, j in enumerate(f):
            pass
        return i + 1


def review_features_sentiment_words(review):
    # creates features for classification based off of sentiment words
    review_tok = word_tokenize(review)
    review_tok = [w for w in review_tok if w not in stop_words]
    review_words = set(review_tok)
    review_bigrams = list(nltk.bigrams(review_tok))
    review_bigrams = set(review_bigrams)
    features = {}
    for word in sent_words:
        features['contains({})'.format(word)] = (word in review_words)
        for neg in negations:
            if (neg, word) in review_bigrams:
                features['negated({})'.format(word)] = True
    return features


def review_features_sentiment_words2(review):
    # creates features for classificaton based off of sentiment words,
    # however, it does not attempt negation
    review_tok = word_tokenize(review)
    review_tok = [w for w in review_tok if w not in stop_words]
    review_words = set(review_tok)
#    review_bigrams = list(nltk.bigrams(review_tok))
#    review_bigrams = set(review_bigrams)
    features = {}
    for word in sent_words:
        features['contains({})'.format(word)] = (word in review_words)
    return features


def review_features_corpus(review, corpus):
    # this creates features for classification based off of the set of words
    # passed to the function under the variable 'corpus'
    # This is based off of the method described in chapter 6 of the NLTK book
    review_tok = word_tokenize(review)
    review_tok = [w for w in review_tok if w not in stop_words]
    review_words = set(review_tok)
    review_bigrams = list(nltk.bigrams(review_tok))
    review_bigrams = set(review_bigrams)
    features = {}
    for word in corpus:
        features['contains({})'.format(word)] = (word in review_words)
        for neg in negations:
            if (neg, word) in review_bigrams:
                features['negated({})'.format(word)] = True
    return features


def review_features_corpus2(review, corpus):
    # this creates features for classification based off of the set of words
    # passed to the function under the variable 'corpus'
    # This method does not attempt to figure out if words are 'negated'
    review_tok = word_tokenize(review)
    review_tok = [w for w in review_tok if w not in stop_words]
    review_words = set(review_tok)
#    review_bigrams = list(nltk.bigrams(review_tok))
#    review_bigrams = set(review_bigrams)
    features = {}
    for word in corpus:
        features['contains({})'.format(word)] = (word in review_words)
    return features


def train_and_test_classifiers(abbreviation):
    a = abbreviation

    # these are used to keep a set of all the words used in all the reviews
    # arto stands for 'all reviews text only'
    arto = ''
    arto_star = ''

    train_reviews = []  # list of training reviews
    test_reviews = []   # list of testing reveiws

    # set up variables to keep track of the number of different reviews in each
    # category to ensure an equal number is used for training and testing
    neg_train_num = 0
    neg_test_num = 0
    pos_train_num = 0
    pos_test_num = 0

    one_train_num = 0
    two_train_num = 0
    three_train_num = 0
    four_train_num = 0
    five_train_num = 0
    one_test_num = 0
    two_test_num = 0
    three_test_num = 0
    four_test_num = 0
    five_test_num = 0

    train_reviews_star = []
    test_reviews_star = []

    # find out which category has the minimum number of reviews and use that as
    # the limit of reviews for every other category
    min_reviews_train = min(file_length('neg_train_' + a + '.json'),
                            file_length('pos_train_' + a + '.json'))
    min_reviews_test = min(file_length('neg_test_' + a + '.json'),
                           file_length('pos_test_' + a + '.json'))
    min_reviews_train_star = min(file_length('one_train_' + a + '.json'),
                                 file_length('two_train_' + a + '.json'),
                                 file_length('three_train_' + a + '.json'),
                                 file_length('four_train_' + a + '.json'),
                                 file_length('five_train_' + a + '.json'))
    min_reviews_test_star = min(file_length('one_test_' + a + '.json'),
                                file_length('two_test_' + a + '.json'),
                                file_length('three_test_' + a + '.json'),
                                file_length('four_test_' + a + '.json'),
                                file_length('five_test_' + a + '.json'))

    print('min_reveiws_train ' + str(min_reviews_train))
    print('min_reveiws_train_star ' + str(min_reviews_train_star))

    # set up lists of training and testing reviews. This is a lot more lines
    # than it needs to be but this helped with clarity while developing
    # one star
    with open('one_train_' + a + '.json', 'r') as one_train:
        for review in one_train:
            one_train_num += 1
            train_reviews_star.append((review, '1'))
            arto_star += review + '\n'
            if one_train_num >= min_reviews_train_star:
                break

    with open('one_test_' + a + '.json', 'r') as one_test:
        for review in one_test:
            one_test_num += 1
            test_reviews_star.append((review, '1'))
            arto_star += review + '\n'
            if one_test_num >= min_reviews_test_star:
                break

    # two star
    with open('two_train_' + a + '.json', 'r') as two_train:
        for review in two_train:
            two_train_num += 1
            train_reviews_star.append((review, '2'))
            arto_star += review + '\n'
            if two_train_num >= min_reviews_train_star:
                break

    with open('two_test_' + a + '.json', 'r') as two_test:
        for review in two_test:
            two_test_num += 1
            test_reviews_star.append((review, '2'))
            arto_star += review + '\n'
            if two_test_num >= min_reviews_test_star:
                break

    # three star
    with open('three_train_' + a + '.json', 'r') as three_train:
        for review in three_train:
            three_train_num += 1
            train_reviews_star.append((review, '3'))
            arto_star += review + '\n'
            if three_train_num >= min_reviews_train_star:
                break

    with open('three_test_' + a + '.json', 'r') as three_test:
        for review in three_test:
            three_test_num += 1
            test_reviews_star.append((review, '3'))
            arto_star += review + '\n'
            if three_test_num >= min_reviews_test_star:
                break

    # four star
    with open('four_train_' + a + '.json', 'r') as four_train:
        for review in four_train:
            four_train_num += 1
            train_reviews_star.append((review, '4'))
            arto_star += review + '\n'
            if four_train_num >= min_reviews_train_star:
                break

    with open('four_test_' + a + '.json', 'r') as four_test:
        for review in four_test:
            four_test_num += 1
            test_reviews_star.append((review, '4'))
            arto_star += review + '\n'
            if four_test_num >= min_reviews_test_star:
                break

    # five star
    with open('five_train_' + a + '.json', 'r') as five_train:
        for review in five_train:
            five_train_num += 1
            train_reviews_star.append((review, '5'))
            arto_star += review + '\n'
            if five_train_num >= min_reviews_train_star:
                break

    with open('five_test_' + a + '.json', 'r') as five_test:
        for review in five_test:
            five_test_num += 1
            test_reviews_star.append((review, '5'))
            arto_star += review + '\n'
            if five_test_num >= min_reviews_test_star:
                break

    # negative reviews
    with open('neg_train_' + a + '.json', 'r') as neg_train:
        for review in neg_train:
            neg_train_num += 1
            train_reviews.append((review, 'neg'))
            arto += review + '\n'
            if neg_train_num >= min_reviews_train:
                break

    with open('neg_test_' + a + '.json', 'r') as neg_test:
        for review in neg_test:
            neg_test_num += 1
            test_reviews.append((review, 'neg'))
            arto += review + '\n'
            if neg_test_num >= min_reviews_test:
                break

    # positive reviews
    with open('pos_train_' + a + '.json', 'r') as pos_train:
        for review in pos_train:
            pos_train_num += 1
            train_reviews.append((review, 'pos'))
            arto += review + '\n'
            if pos_train_num >= neg_train_num:
                break

    with open('pos_test_' + a + '.json', 'r') as pos_test:
        for review in pos_test:
            pos_test_num += 1
            test_reviews.append((review, 'pos'))
            arto += review + '\n'
            if pos_test_num >= neg_test_num:
                break

    # get most common words from reviews used in positive and negative reveiws
    # this is based on the method described in chapter 6 of the NLTK book
    all_reviews_text = arto.translate(table)
    all_reviews_text = word_tokenize(all_reviews_text)

    all_reviews_text = [w for w in all_reviews_text if w not in stop_words]

    all_words = nltk.FreqDist(w for w in all_reviews_text)
    word_features = [i[0] for i in all_words.most_common(2000)]
    # I used the 2000 most common words as I found it had the highest accuracy
    # when performing testing
    print('set up word features')

    # get most common words from reveiws used in reviews sorted into individual
    # star ratings. Again, this is based on the method described in chapter 6
    # of the NLTK book
    all_reviews_text_star = arto_star.translate(table)
    all_reviews_text_star = word_tokenize(all_reviews_text_star)

    all_reviews_text_star = [w for w in all_reviews_text_star
                             if w not in stop_words]

    all_words_star = nltk.FreqDist(w for w in all_reviews_text_star)
    word_features_star = [i[0] for i in all_words_star.most_common(500)]
    # I used 500 words here as in my testing this yielded the highest accuracy
    print('set up word features star')

    # create all featuresets that will be used
    # create featureset using words from indiv. star reviews
    train_featureset_corpus_star = (
            [(review_features_corpus(text, word_features_star), attitude)
                for (text, attitude) in train_reviews_star])
    print('set up train featureset corpus star')
    test_featureset_corpus_star = (
            [(review_features_corpus(text, word_features_star), attitude)
             for (text, attitude) in test_reviews_star])
    print('set up test featureset corpus star')

    train_featureset_corpus_star2 = (
            [(review_features_corpus2(text, word_features_star), attitude)
                for (text, attitude) in train_reviews_star])
    print('set up train featureset corpus star 2')
    test_featureset_corpus_star2 = (
            [(review_features_corpus2(text, word_features_star), attitude)
             for (text, attitude) in test_reviews_star])
    print('set up test featureset corpus star 2')

    # create featureset using sentiment words for indiv. star reviews
    train_sent_featureset_star = (
            [(review_features_sentiment_words(text), attitude)
             for (text, attitude) in train_reviews_star])
    print('set up train sent featureset star')
    test_sent_featureset_star = (
            [(review_features_sentiment_words(text), attitude)
             for (text, attitude) in test_reviews_star])
    print('set up test sent featureset star')

    train_sent_featureset_star2 = (
            [(review_features_sentiment_words2(text), attitude)
             for (text, attitude) in train_reviews_star])
    print('set up train sent featureset star 2')
    test_sent_featureset_star2 = (
            [(review_features_sentiment_words2(text), attitude)
             for (text, attitude) in test_reviews_star])

    # create featureset using words from positive and negative reviews
    train_featureset_corpus = (
            [(review_features_corpus(text, word_features), attitude)
                for (text, attitude) in train_reviews])
    print('set up train featureset corpus')
    test_featureset_corpus = (
            [(review_features_corpus(text, word_features), attitude)
             for (text, attitude) in test_reviews])
    print('set up test featureset corpus')

    train_featureset_corpus2 = (
            [(review_features_corpus2(text, word_features), attitude)
                for (text, attitude) in train_reviews])
    print('set up train featureset corpus2')
    test_featureset_corpus2 = (
            [(review_features_corpus2(text, word_features), attitude)
             for (text, attitude) in test_reviews])
    print('set up test featureset corpus2')

    # generate featuresets using all sentiment words
    train_sent_featureset = [(review_features_sentiment_words(text), attitude)
                             for (text, attitude) in train_reviews]
    print('set up train sent featureset')
    test_sent_featureset = [(review_features_sentiment_words(text), attitude)
                            for (text, attitude) in test_reviews]
    print('set up test sent featureset')

    train_sent_featureset2 = (
            [(review_features_sentiment_words2(text), attitude)
             for (text, attitude) in train_reviews])
    print('set up train sent featureset 2')
    test_sent_featureset2 = (
            [(review_features_sentiment_words2(text), attitude)
             for (text, attitude) in test_reviews])
    print('set up test sent featureset 2')

    print('set up featuresets')

    print('starting classification')

    print('classifier_corpus_star')
    classifier_corpus_star = nltk.NaiveBayesClassifier.train(
            train_featureset_corpus_star)
    print('checking accuracy corpus star method')
    print(nltk.classify.accuracy(
            classifier_corpus_star, test_featureset_corpus_star))
    print(classifier_corpus_star.show_most_informative_features(5))

    print('classifier_corpus_star2')
    classifier_corpus_star2 = nltk.NaiveBayesClassifier.train(
            train_featureset_corpus_star2)
    print('checking accuracy corpus star method 2')
    print(nltk.classify.accuracy(
            classifier_corpus_star2, test_featureset_corpus_star2))
    print(classifier_corpus_star2.show_most_informative_features(5))

    print('classifier sent star')
    classifier_sent_star = nltk.NaiveBayesClassifier.train(
            train_sent_featureset_star)
    print('Checking accuracy sent words star')
    print(nltk.classify.accuracy(
            classifier_sent_star, test_sent_featureset_star))
    print(classifier_sent_star.show_most_informative_features(5))

    print('classifier sent star 2')
    classifier_sent_star2 = nltk.NaiveBayesClassifier.train(
            train_sent_featureset_star2)
    print('Checking accuracy method sent star 2')
    print(nltk.classify.accuracy(
            classifier_sent_star2, test_sent_featureset_star2))
    print(classifier_sent_star2.show_most_informative_features(5))

    print('classifier_corpus pos - neg')
    classifier_corpus = nltk.NaiveBayesClassifier.train(
            train_featureset_corpus)
    print('checking accuracy corpus pos - neg method')
    print(nltk.classify.accuracy(classifier_corpus, test_featureset_corpus))
    print(classifier_corpus.show_most_informative_features(5))

    print('classifier_corpus2 pos - neg')
    classifier_corpus2 = nltk.NaiveBayesClassifier.train(
            train_featureset_corpus2)
    print('checking accuracy corpus pos - neg method 2')
    print(nltk.classify.accuracy(classifier_corpus2, test_featureset_corpus2))
    print(classifier_corpus2.show_most_informative_features(5))

    print('classifier sent pos - neg')
    classifier_sent = nltk.NaiveBayesClassifier.train(train_sent_featureset)
    print('Checking accuracy sent words pos - neg')
    print(nltk.classify.accuracy(classifier_sent, test_sent_featureset))
    print(classifier_sent.show_most_informative_features(5))

    print('classifier sent pos - neg 2')
    classifier_sent2 = nltk.NaiveBayesClassifier.train(train_sent_featureset2)
    print('Checking accuracy method sent pos - neg 2')
    print(nltk.classify.accuracy(classifier_sent2, test_sent_featureset2))
    print(classifier_sent2.show_most_informative_features(5))

    return (classifier_corpus_star, classifier_corpus_star2,
            classifier_sent_star, classifier_sent_star2, classifier_corpus,
            classifier_corpus2, classifier_sent, classifier_sent2,
            word_features, word_features_star)


if __name__ == '__main__':
    # use the same file abbreviation that was used to create the other files
    start_time = time.time()
    (corpus_star, corpus_star2, sent_star,
     sent_star2, corpus_pos_neg, corpus_pos_neg2,
     sent_pos_neg, sent_pos_neg2, word_features,
     word_features_star) = train_and_test_classifiers('auto')
    print('Took %s seconds to run' % (time.time() - start_time))
