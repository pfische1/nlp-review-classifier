# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 15:02:26 2019

@author: ptfis
"""

# this file creates training and testing files from the files generated in
# gz_to_json_conversion.py

import random
import json

# TODO more detailed documentation


def generate_train_test(abbreviation):
    # generates training and testing files from files created in
    # gz_to_json_conversion.py using the same abbreviation

    a = abbreviation
    # get lists of all the different reviews
    with open('pos_' + a + '_reviews.json', 'r') as pos_reviews:
        pos_reviews_list = []
        for line in pos_reviews:
            line = json.loads(line)
            pos_reviews_list.append(line)

    with open('neg_' + a + '_reviews.json', 'r') as neg_reviews:
        neg_reviews_list = []
        for line in neg_reviews:
            line = json.loads(line)
            neg_reviews_list.append(line)

    with open('one_' + a + '_reviews.json', 'r') as one_reviews:
        one_reviews_list = []
        for line in one_reviews:
            line = json.loads(line)
            one_reviews_list.append(line)

    with open('two_' + a + '_reviews.json', 'r') as two_reviews:
        two_reviews_list = []
        for line in two_reviews:
            line = json.loads(line)
            two_reviews_list.append(line)

    with open('three_' + a + '_reviews.json', 'r') as three_reviews:
        three_reviews_list = []
        for line in three_reviews:
            line = json.loads(line)
            three_reviews_list.append(line)

    with open('four_' + a + '_reviews.json', 'r') as four_reviews:
        four_reviews_list = []
        for line in four_reviews:
            line = json.loads(line)
            four_reviews_list.append(line)

    with open('five_' + a + '_reviews.json', 'r') as five_reviews:
        five_reviews_list = []
        for line in five_reviews:
            line = json.loads(line)
            five_reviews_list.append(line)

    # shuffle reviews to get random order
    random.shuffle(pos_reviews_list)
    random.shuffle(neg_reviews_list)
    random.shuffle(one_reviews_list)
    random.shuffle(two_reviews_list)
    random.shuffle(three_reviews_list)
    random.shuffle(four_reviews_list)
    random.shuffle(five_reviews_list)

    # take 80% of file for training and 20% for testing
    one_test_num = len(one_reviews_list) // 5
    two_test_num = len(two_reviews_list) // 5
    three_test_num = len(three_reviews_list) // 5
    four_test_num = len(four_reviews_list) // 5
    five_test_num = len(five_reviews_list) // 5

    neg_test_num = len(neg_reviews_list) // 5
    pos_test_num = len(pos_reviews_list) // 5

    # split the lists into training and testing sets
    one_test = one_reviews_list[:one_test_num]
    two_test = two_reviews_list[:two_test_num]
    three_test = three_reviews_list[:three_test_num]
    four_test = four_reviews_list[:four_test_num]
    five_test = five_reviews_list[:five_test_num]

    neg_test = neg_reviews_list[:neg_test_num]
    pos_test = pos_reviews_list[:pos_test_num]

    one_train = one_reviews_list[one_test_num:]
    two_train = two_reviews_list[two_test_num:]
    three_train = three_reviews_list[three_test_num:]
    four_train = four_reviews_list[four_test_num:]
    five_train = five_reviews_list[five_test_num:]

    neg_train = neg_reviews_list[neg_test_num:]
    pos_train = pos_reviews_list[pos_test_num:]

    # Write the summary of the reveiw and the text of the review to a file
    with open('one_train_' + a + '.json', 'w') as one_train_file:
        for review in one_train:
            one_train_file.write(review['summary'] + ' - '
                                 + review['reviewText'] + '\n')

    with open('two_train_' + a + '.json', 'w') as two_train_file:
        for review in two_train:
            two_train_file.write(review['summary'] + ' - '
                                 + review['reviewText'] + '\n')

    with open('three_train_' + a + '.json', 'w') as three_train_file:
        for review in three_train:
            three_train_file.write(review['summary'] + ' - '
                                   + review['reviewText'] + '\n')

    with open('four_train_' + a + '.json', 'w') as four_train_file:
        for review in four_train:
            four_train_file.write(review['summary'] + ' - '
                                  + review['reviewText'] + '\n')

    with open('five_train_' + a + '.json', 'w') as five_train_file:
        for review in five_train:
            five_train_file.write(review['summary'] + ' - '
                                  + review['reviewText'] + '\n')

    # test files for individual star ratings
    with open('one_test_' + a + '.json', 'w') as one_test_file:
        for review in one_test:
            one_test_file.write(review['summary'] + ' - '
                                + review['reviewText'] + '\n')

    with open('two_test_' + a + '.json', 'w') as two_test_file:
        for review in two_test:
            two_test_file.write(review['summary'] + ' - '
                                + review['reviewText'] + '\n')

    with open('three_test_' + a + '.json', 'w') as three_test_file:
        for review in three_test:
            three_test_file.write(review['summary'] + ' - '
                                  + review['reviewText'] + '\n')

    with open('four_test_' + a + '.json', 'w') as four_test_file:
        for review in four_test:
            four_test_file.write(review['summary'] + ' - '
                                 + review['reviewText'] + '\n')

    with open('five_test_' + a + '.json', 'w') as five_test_file:
        for review in five_test:
            five_test_file.write(review['summary'] + ' - '
                                 + review['reviewText'] + '\n')

    with open('pos_train_' + a + '.json', 'w') as pos_train_file:
        for review in pos_train:
            pos_train_file.write(review['summary'] + ' - '
                                 + review['reviewText'] + '\n')

    with open('pos_test_' + a + '.json', 'w') as pos_test_file:
        for review in pos_test:
            pos_test_file.write(review['summary'] + ' - '
                                + review['reviewText'] + '\n')

    with open('neg_test_' + a + '.json', 'w') as neg_test_file:
        for review in neg_test:
            neg_test_file.write(review['summary'] + ' - '
                                + review['reviewText'] + '\n')

    with open('neg_train_' + a + '.json', 'w') as neg_train_file:
        for review in neg_train:
            neg_train_file.write(review['summary'] + ' - '
                                 + review['reviewText'] + '\n')


if __name__ == '__main__':
    generate_train_test('auto')
